from flask import Flask
from datetime import datetime
import time

app = Flask(__name__)

def get_time_string():
    current_time = datetime.now().strftime("%H:%M:%S")
    seconds = int(current_time.split(":")[2])
    return current_time, "ODD" if seconds % 2 != 0 else "EVEN"

@app.route('/date')
def date():
    current_time, parity = get_time_string()
    return f"{current_time} {parity}"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

